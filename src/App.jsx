import { useState, useEffect } from "react";
import HoldingTable from "./HoldingTable";
import { Box } from "@chakra-ui/react";

function App() {
  const [allData, setAllData] = useState([]);

  useEffect(() => {
    fetch("https://canopy-frontend-task.now.sh/api/holdings")
      .then((res) => res.json())
      .then((data) => {
        setAllData(data.payload);
      });
  }, []);

  return (
    <Box pt={6}>
      <HoldingTable allData={allData} />
    </Box>
  );
}

export default App;
