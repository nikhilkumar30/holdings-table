import React, { useState } from "react";
import { Flex, Table, Tr, Td, Th, Tbody, Text } from "@chakra-ui/react";
import { TriangleDownIcon, TriangleUpIcon } from "@chakra-ui/icons";

const HoldingTable = ({ allData }) => {
  const [expandedRows, setExpandedRows] = useState([]);
  const [clickedAsset, setClickedAsset] = useState(null);

  const toggleRow = (assetClass) => {
    setClickedAsset(assetClass);
    setExpandedRows((prevExpandedRows) =>
      prevExpandedRows.includes(assetClass)
        ? prevExpandedRows.filter((row) => row !== assetClass)
        : [...prevExpandedRows, assetClass]
    );
  };

  const isExpanded = (assetClass) => expandedRows.includes(assetClass);

  const groupedHoldings = allData.reduce((acc, holding) => {
    const assetClassName = holding.asset_class.toUpperCase();
    acc[assetClassName] = acc[assetClassName] || [];
    acc[assetClassName].push(holding);
    return acc;
  }, {});

  return (
    <Flex
      direction="column"
      m={6}
      p={6}
      bg="white"
      borderRadius={6}
      overflowX="auto"
    >
      <Table variant="simple">
        <Tbody>
          {Object.keys(groupedHoldings).map((assetClass, index) => (
            <React.Fragment key={assetClass}>
              <Tr
                onClick={() => toggleRow(assetClass)}
                borderRadius="8px"
                cursor="pointer"
              >
                <Td>
                  {isExpanded(assetClass) ? (
                    <TriangleUpIcon color="#F2613F" />
                  ) : (
                    <TriangleDownIcon color="#F2613F" />
                  )}
                  <b style={{ marginLeft: "0.5rem" }}>
                    {assetClass} ({groupedHoldings[assetClass].length})
                  </b>
                </Td>
              </Tr>
              {isExpanded(assetClass) && (
                <>
                  <Tr>
                    <Th>
                      <Text fontFamily="Sedan" fontSize="md">
                        Name
                      </Text>
                    </Th>
                    <Th>
                      <Text fontFamily="Sedan" fontSize="md">
                        Ticker
                      </Text>
                    </Th>
                    <Th>
                      <Text fontFamily="Sedan" fontSize="md">
                        Avg Price
                      </Text>
                    </Th>
                    <Th>
                      <Text fontFamily="Sedan" fontSize="md">
                        Market Price
                      </Text>
                    </Th>
                    <Th>
                      <Text fontFamily="Sedan" fontSize="md">
                        Latest Change (%)
                      </Text>
                    </Th>
                    <Th>
                      <Text fontFamily="Sedan" fontSize="md">
                        Market Value (Base CCY)
                      </Text>
                    </Th>
                  </Tr>
                  {groupedHoldings[assetClass].map((holding, index) => (
                    <Tr key={index}>
                      <Td>
                        <Text fontFamily="Sedan" fontSize="md">
                          {holding.name}
                        </Text>
                      </Td>
                      <Td>
                        <Text fontFamily="Sedan" fontSize="md">
                          {holding.ticker}
                        </Text>
                      </Td>
                      <Td>
                        <Text fontFamily="Sedan" fontSize="md">
                          {holding.avg_price}
                        </Text>
                      </Td>
                      <Td>
                        <Text fontFamily="Sedan" fontSize="md">
                          {holding.market_price}
                        </Text>
                      </Td>
                      <Td>
                        <Text fontFamily="Sedan" fontSize="md">
                          {holding.latest_chg_pct}
                        </Text>
                      </Td>
                      <Td>
                        <Text fontFamily="Sedan" fontSize="md">
                          {holding.market_value_ccy}
                        </Text>
                      </Td>
                    </Tr>
                  ))}
                </>
              )}
            </React.Fragment>
          ))}
        </Tbody>
      </Table>
    </Flex>
  );
};

export default HoldingTable;
